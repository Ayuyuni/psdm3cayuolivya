<!DOCTYPE html>
<html>
  <head>
    <style>
      h1{
        text-align: center;
      }
      .container{
        width: 400px;
        margin: auto;
      }
    </style>
  </head>
  <body>
    <h1>Input Data Pegawai</h1>
    <div class="container">
      <form id="form_pegawai" action="input_proses.php" method="post">
        <fieldset>
        <legend><b> = Input Data Pegawai = </b></legend>
          <p>
            <label for="nik">NIK : </label>
            <input type="text" name="nik" id="nik" placeholder="contoh: 17001">
          </p>
          <p>
            <label for="nama_lengkap">Nama Lengkap: </label>
            <input type="text" name="nama_lengkap" id="nama_lengkap">
          </p>
		            <p>
            <label for="glar_dpn">Gelar Depan: </label>
            <input type="text" name="glar_dpan" id="glar_dpan">
          </p>
		            <p>
            <label for="glar_blkg">Gelar Blakang: </label>
            <input type="text" name="glar_blkg" id="glar_blkg">
          </p>
          <p>
            <label for="identitas" >Identitas : </label>
              <select name="identitas" id="identitas">
                <option value="KTP">KTP </option>
                <option value="SIM">SIM</option>

              </select>
          </p>
          <p>
            <label for="no_id">No Id : </label>
            <input type="text" name="no_id" id="no_id placeholder="Contoh: 100001">
          </p>
          <p >
            <label for="tpt_lhr">Tmpt Lahir : </label>
            <input type="text" name="tpt_lhr" id="tpt_lhr" placeholder="Contoh: Solo">
          </p>
          <p>
            <label for="tgl_lhr">Tgl Lahir: </label>
            <input type="text" name="tgl_lhr" id="tgl_lhr placeholder="Contoh: TTTT-BB-HH">
          </p>
		            <p>
            <label for="jenkel" >Jenis Kelamin: </label>
              <select name="jenkel" id="jenkel">
                <option value="LAKI">Laki </option>
                <option value="PEREMPUAN">Perempuan</option>

              </select>
          </p>
		  
		            <p>
            <label for="telepon">Telepon: </label>
            <input type="text" name="telepon" id="telepon">
          </p>
		            <p>
            <label for="hp">HP: </label>
            <input type="text" name="hp" id="hp">
          </p>
		            <p>
            <label for="email">Email: </label>
            <input type="text" name="email" id="email">
          </p>
		            <p>
            <label for="website">Website: </label>
            <input type="text" name="website" id="website">
          </p>
        </fieldset>
        <p>
          <input type="submit" name="input" value="Tambah Data">
        </p>
      </form>
    </div>
  </body>
</html>