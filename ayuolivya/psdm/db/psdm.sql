-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 19, 2017 at 01:27 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `psdm`
--

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(10) NOT NULL,
  `nik` int(10) DEFAULT NULL,
  `nama_lengkap` varchar(25) DEFAULT NULL,
  `glar_dpan` varchar(15) DEFAULT NULL,
  `glar_blkg` varchar(15) DEFAULT NULL,
  `identitas` varchar(10) DEFAULT NULL,
  `no_id` int(20) DEFAULT NULL,
  `tpt_lhr` varchar(20) DEFAULT NULL,
  `tgl_lhr` date DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `hp` varchar(15) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `website` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `nik`, `nama_lengkap`, `glar_dpan`, `glar_blkg`, `identitas`, `no_id`, `tpt_lhr`, `tgl_lhr`, `jenkel`, `telepon`, `hp`, `email`, `website`) VALUES
(1, 17001, 'Tri Marzuki', 'Prof', 'M.Kom', 'KTP', 100001, 'Karanganyar', '1992-07-10', 'LAKI', '081906415825', '081906415825', 'ohmjuki@gmail.com', 'marzuki.com'),
(2, 17002, 'Fitriani', 'Dr', 'M.Kom', 'KTP', 100002, 'Solo', '1996-10-10', 'PEREMPUAN', '081906415828', '081906415828', 'oooo@gmail.com', 'oooo.com'),
(3, 17003, 'Mila', 'DR', 'S.Pd', 'SIM', 100003, 'Boyolali', '1992-10-10', 'PEREMPUAN', '082906415828', '082906415828', 'mila@gmail.com', 'mila.com'),
(4, 17004, 'Wildan', 'Drs', 'S.Ag', 'SIM', 100004, 'Sragen', '1992-01-10', 'LAKI', '082906415825', '082906415825', 'wildan@gmail.com', 'wildan.com'),
(5, 17005, 'Ari', 'H', 'S.Ag', 'KTP', 100005, 'Klaten', '1993-10-17', 'LAKI', '082906415825', '081906415825', 'ari@gmail.com', 'ari.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
